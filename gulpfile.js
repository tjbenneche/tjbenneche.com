/*global require*/
"use strict"

var gulp = require('gulp'),
    path = require('path'),
    data = require('gulp-data'),
    pug = require('gulp-pug'),
    prefix = require('gulp-autoprefixer'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync')


// Directories
var paths = {
  public: 'public/',
  sass: 'src/sass/',
  css: 'public/css/',
  data: 'src/data/',
  img: 'public/img/',
  js: 'public/js/'
}


gulp.task('pug', function() {
  return gulp.src('src/**/*.pug')
    // .pipe(data(function(file) {
    //   return require(paths.data + path.basename(file.path) + '.json')
    // }))
    .pipe(pug())
    .pipe(gulp.dest(paths.public))
})

gulp.task('rebuild', ['pug'], function() {
  browserSync.reload()
})

gulp.task('img', function() {
  return gulp.src('src/img/*')
    .pipe(gulp.dest(paths.img))
})

gulp.task('js', function() {
  return gulp.src('src/js/*')
    .pipe(gulp.dest(paths.js))
})

gulp.task('browser-sync', ['sass', 'pug'], function() {
  browserSync({
    server: {
      baseDir: paths.public
    },
    notify: false
  })
})

gulp.task('sass', function() {
  return gulp.src(paths.sass + '*.scss')
    .pipe(sass({
      includePaths: [paths.sass],
      outputStyle: 'compressed'
    }))
    .on('error', sass.logError)
    .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
      cascade: true
    }))
    .pipe(gulp.dest(paths.css))
    .pipe(browserSync.reload({
      stream: true
    }))
})

gulp.task('watch', function() {
  gulp.watch(paths.sass + '*.scss', ['sass'])
  gulp.watch(paths.sass + '/globals/*.scss', ['sass'])
  gulp.watch('src/**/*.pug', ['rebuild'])
  gulp.watch('src/img/*', ['img'])
  gulp.watch('src/js/*', ['js'])
})

gulp.task('build', ['sass', 'pug', 'img', 'js'])

gulp.task('default', ['browser-sync', 'build', 'watch'])
